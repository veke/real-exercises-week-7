
//näkymä käyttäjälle syötteestä eli checkboxi, taski ja painike johon poisto ominaisuus laitetaan
const Todo = ( {todo, removefunktio} ) => {

    const handleRemove = () => {
        removefunktio(todo.id)
    }
    
    return(
      <div>
        <input type="checkbox"/>
        <li>{todo.task}</li>
        <button onClick={handleRemove}>Delete</button>
      </div>
    );
}


export default Todo;