import { useEffect, useContext } from 'react'
import { getAllProducts } from '../../services/storeService'
import { AppContext } from "../../ContextProvider";
import { useNavigate } from 'react-router-dom';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import Rating from '@mui/material/Rating';
import './allProducts.css'


const AllProducts = () => {
    const navigate = useNavigate();
    const [state, setState] = useContext(AppContext);
    //console.log(state)

    useEffect(() => {
        if (state.products.length === 0) {
            const getProducts = async () => {
                const data = await getAllProducts()
                setState((state) => {return {...state, products: data}})
            }
            getProducts()
        }
        
    }, [])

    const handleClick = (event, id) => {
        event.preventDefault();
        //console.log(id)
        setState((state) => {return {...state, id: id}})
        navigate(`/products/${id}`)
    }

    return (
        <div className='allproducts'>
            <Typography variant="h3">{state.category}</Typography>
            <div style={{ display: "flex", flexWrap: "wrap" }}>
                {state.products.filter((product) => product.category === state.category || state.category === "All products").map((product) => {
                    return <ol>
                        <Card onClick={event => handleClick(event, product.id)}
                        className='products-div'
                        key={product.id}
                        sx={{ width: 400, height: 900 }}
                        >
                            <CardMedia component='img' 
                                image={product.image}
                                alt='productpicture'
                                height='300'>
                            </CardMedia>
                            <CardContent>
                                <Typography gutterBottom variant="h5" component="div">{product.title}</Typography>
                                <Typography variant="h6" color="text.primary">{product.price} €</Typography>
                                <Typography variant="body2" color="text.secondary">{product.description}</Typography>
                                <Rating name="half-rating" precision={0.5} value={product.rating ? product.rating.rate : 0} readOnly />
                            </CardContent>
                    </Card>
                    </ol>
                })}
            </div>
        </div>
    )
}

export default AllProducts