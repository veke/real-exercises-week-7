import { useContext } from 'react';
import { useNavigate } from 'react-router-dom';
import { AppContext } from '../../ContextProvider';
import { addNewProduct } from '../../services/storeService';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import './addproduct.css'



const AddProduct = () => {
    const navigate = useNavigate()
    const [state, setState] = useContext(AppContext)

    const productadding = async (event) => {
        event.preventDefault()


        const newProduct = {
            title: event.target.title.value,
            price: event.target.price.value,
            description: event.target.description.value,
            category: event.target.category.value,
            image: event.target.image.value
        }
        //tässä pitäis lähettää bakkarille
        const response = await addNewProduct(newProduct)
        //console.log(response)
        console.log(state.products)
        setState((state) => {return {...state, products: [...state.products, response]}})
        navigate('/products')

    }

    /*
    let objekti = {
        categories: ['kivaa', 'hyvää', 'paskaa'],
        products: ['ekatuote', 'tokatuote']
      }
      
      uusiobjekti = {
        ...objekti,
        products: [...objekti.products, 'uusituote']
      }
    */


    return(
        <form onSubmit={e => productadding(e)}>
            <TextField style={{marginRight: '7px'}} className='field' id="outlined-basic" label="Title" variant="outlined" name='title'/>
            <TextField style={{marginRight: '7px'}} className='field' id="outlined-basic" label="Price" variant="outlined" name='price'/>
            <TextField style={{marginRight: '7px'}} className='field' id="outlined-basic" label="Description" variant="outlined" name='description'/>
            <TextField style={{marginRight: '7px'}} className='field' id="outlined-basic" label="Category" variant="outlined" name='category'/>
            <TextField style={{marginRight: '7px'}} className='field' id="outlined-basic" label="Image" variant="outlined" name='image' value='https://fakestoreapi.com/img/61IBBVJvSDL._AC_SY879_.jpg'/>
            <Button className='submitproductbutton' variant="contained" color="success" type='submit'>Submit adding</Button>
        </form>
    )
}


export default AddProduct