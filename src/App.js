import { 
  Routes, 
  BrowserRouter as Router, 
  Route,
  Link
} from 'react-router-dom'

import './App.css';
import Dropdown from './pages/dropdown/Dropdown'
import AllProducts from './pages/allProducts/AllProducts'
import OneProduct from './pages/oneProduct/OneProduct'
import ContextProvider from './ContextProvider';
import AddProduct from './pages/addProduct/AddProduct';
import Typography from '@mui/material/Typography';

//import background from './apina.jpeg'
//<Route path='/' element={<Home/>}></Route>
//<Link className='nav-home' to='/'></Link>



const App = () => {
  return (
    <div className='App'>
      <div>
        <Typography variant="h2" component="div">
          Welcome to FAKESTORE
        </Typography>
      </div>
      <Router>
        <ContextProvider>
        <nav>

        </nav>
        <Dropdown/>
        <Routes>
          <Route path='/products' element={<AllProducts/>}></Route>
          <Route path='/products/:id' element={<OneProduct/>}></Route>
          <Route path='/add' element={<AddProduct/>}></Route>
        </Routes>
        <footer></footer>
        </ContextProvider>
      </Router>
    </div>
  )
}

















/* TÄMÄ ON TODOS TEHTÄVÄ EKALTA PÄIVÄLTÄ
//tässä vain lisätään aina uusi alkio listan kärkeen
const App = () => {
  const [todos, setTodos] = useState([])
  
  const addTodo = (todo) => {
    setTodos([todo, ...todos]);
  }

  const deleteTodo = (id) => {
    const remainingTodos = todos.filter((todo) => todo.id !== id)
    setTodos(remainingTodos)
  }

  return (
    <div className="App">
      <TodoForm addTodo={addTodo}/> 
      <TodoList todos={todos} removefunktio={deleteTodo}/>
    </div>
  );
}
*/

export default App;
