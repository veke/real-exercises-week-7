import { useState, createContext } from 'react';

export const AppContext = createContext([{}, () => {}]); // Create context

const ContextProvider = props => { // Provider provides child components with props
    const [state, setState] = useState({category: "All products", products: [], id: []});
    return (
        <AppContext.Provider value={[state, setState]}>
            {props.children}
        </AppContext.Provider>
    );
};

export default ContextProvider