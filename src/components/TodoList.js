import Todo from "./Todo.js";

//mapataan todosit ul:ksi ja viedään ne Todo funktiolle 
const TodoList = ( {todos, removefunktio} ) => {
  
    return (
      <ul>
        {todos.map(todo =>(
          <Todo key={todo.id} todo={todo} removefunktio={removefunktio}/>
        ))}
      </ul>
    )
}


export default TodoList;