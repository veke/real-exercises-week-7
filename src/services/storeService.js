import axios from 'axios'

const baseURL = 'https://fakestoreapi.com/products'

export const getAllProducts = async () => {
    const result = await axios.get(`${baseURL}`)
    return result.data
}


export const getCategories = async () => {
    const result = await axios.get(`${baseURL}/categories`)
    return result.data
}


export const getOneProduct = async (id) => {
    const result = await axios.get(`${baseURL}/${id}`)
    return result.data
}

export const addNewProduct = async (prod) => {
    const result = await axios.post(`${baseURL}`, prod)
    return result.data
}