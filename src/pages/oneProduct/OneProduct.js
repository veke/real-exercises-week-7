import { useState, useEffect, useContext } from 'react'
import { getOneProduct } from '../../services/storeService'
import { useParams } from 'react-router-dom'
import { AppContext } from "../../ContextProvider";
import Typography from '@mui/material/Typography';
import Rating from '@mui/material/Rating';

const OneProduct = () => {
    const params = useParams();
    const [state, setState] = useContext(AppContext);
    //const [product, setProduct] = useState(null)
    //console.log(state.id)

    /*
    useEffect(() => {
        const getProduct = async () => {
            const data = await getOneProduct(params.id)
            //console.log(params)
            setProduct(data)
        }
        getProduct()
    }, [])
    */

    
    const filterproduct = state.products.filter((tuote) => tuote.id === state.id)
    console.log(filterproduct[0])
    const product = filterproduct[0]
    

    /*
    const handleDelete = (event) => {
        event.preventDefault()
        //logiikka
    }

    const handleUpdate = (event) => {
        event.preventDefault()
        //logiikka
    }

        <button onClick={e => handleDelete(e)}>Delete</button>
        <button onClick={e => handleUpdate(e)}>Update</button>

    */

    if (product === null) {
        return <div>Loading...</div>
    }

    return(
        <div className='singleproduct' key={product.id}>
            <Typography variant='h3'>{product.title}</Typography>
            <img src={product.image}
            alt='productpicture'
            width="250"
            height="300"></img>
            <Typography variant="h6">{product.price} €</Typography>
            <Typography variant="body2">{product.description}</Typography>
            <Rating name="half-rating" precision={0.5} value={product.rating.rate} readOnly/>
        </div>
    )
}


export default OneProduct