import { useEffect, useState, useContext } from "react";
import { getCategories } from "../../services/storeService";
import { AppContext } from "../../ContextProvider";
import { useNavigate } from "react-router-dom";
import Button from '@mui/material/Button';
import Select from '@mui/material/Select';
import FormControl from '@mui/material/FormControl';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import './dropdown.css'

const Dropdown = () => {
    const navigate = useNavigate()
    const [state, setState] = useContext(AppContext);
    const [categories, setCategories] = useState([]);

    useEffect(() => {
        const allCategories = async () => {
            const data = await getCategories();
            setCategories(["All products", ...data]);
        }
        allCategories();
    },[])

    const handleriFunktio = (event) => {
        event.preventDefault()
        const kategoria = event.target.value
        setState((state) => {return {...state, category: kategoria}})
    }

    const handleAdding = (e) => {
        e.preventDefault();
        navigate('/add')

    }
    
    return(
        <div>
         <FormControl sx={{ m: 1, minWidth: 200 }}>
            <InputLabel id="demo-simple-select-label">Categories</InputLabel>
            <Select onChange={e => handleriFunktio(e)} labelId="demo-simple-select-label" label="Categories" >
            {categories.map((category) => {
            return <MenuItem value={category}>{category}</MenuItem>
            })}
            </Select>
         </FormControl>
         <Button className='addnewproductbutton' variant="contained" onClick={e => handleAdding(e)}>Add new product</Button>
        </div>
    )
}

export default Dropdown