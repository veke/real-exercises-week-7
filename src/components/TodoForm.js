import { v4 as uuidv4 } from 'uuid';

const TodoForm = ( {addTodo} ) => {
    //const addTodo = props.addTodo
    //addataan syöte ja tiedot sekä alustetaan pohja taas valmiiksi seuraavaa varten
    const handleSubmit = (e) => {
      e.preventDefault(); //estää refreshin vaikka eventti tapahtuisi
      console.log(e.target.task.value)
      const task = e.target.task.value
      if (task) {
        addTodo({task: task, id: uuidv4(), completed: false})
      }
    }
  
    return (
      <form onSubmit={handleSubmit}>
        <input 
          name="task"
          type="text"
        />
        <button type="submit">Submit</button>
      </form>
    );
}

export default TodoForm;
